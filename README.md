## <span style="color:#4863A0">KinD - Nginx, Flask and Mongo orchestration </span>
<div align="center">
	<img src="https://d33wubrfki0l68.cloudfront.net/d0c94836ab5b896f29728f3c4798054539303799/9f948/logo/logo.png" align="center" width="300" alt="Project icon" />
</div>

**Criteria for work:**

1. Establishing a connection between the user, the server and the database, when only the client side is exposed to the outside world.
2. User information will be stored on a separate configmap.
3. MongoDB URI password will be stored in a secret in base64.
4. The connection to the pods will be established via services only.
5. The MongoDB will run as statefulset with the following requirements:
   1. Use of  PVC's and Storage Classes for persistence
   2. Authentication passwords will be stored in a dedicated secret in base64
   3. Forceful definition secondary and primary pods.
6. Straight forward labelling
7. Resource limits
8. Configure all applications to run redundantly.
9. Individual node for each application, while the db pods will not run on the same node if
   there is more then one.



**Table of content:**

[TOC]

> This part is based on the applications and Docker implementations shown in the "Web App In Docker Compose".
>

------



### 0 / Establishing the cluster

**cluster-config.yaml:**

```yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
- role: worker
- role: worker
```

> In this case each node will run different application based on the labelling and the affinity of the deployments/ stetefulset.
>



1. creating the cluster:

   ```shell
   $ sudo kind create cluster --name <cluster_name> --config=cluster-config.yaml
   ```

2. importing the relevant images:
    ```shell
    $ sudo kind load docker-image <image name> --name <cluster_name>
    ```



At this point the cluster should look like this:

```shell
$ sudo kubctl get nodes
```

```shell
NAME                     STATUS   ROLES           AGE   VERSION
exchange-control-plane   Ready    control-plane   22h   v1.24.0
exchange-worker          Ready    <none>          22h   v1.24.0
exchange-worker2         Ready    <none>          22h   v1.24.0
exchange-worker3         Ready    <none>          22h   v1.24.0
exchange-worker4         Ready    <none>          22h   v1.24.0
```



3. Lable the nodes accordngly to the wanted pod affinity (deployments) :

   ```shell
   $ sudo kubectl label nodes exchange-worker special-node-label=frontend-node
   $ sudo kubectl label nodes exchange-worker2 special-node-label=backend-node
   ```

------



### 1 / Front-end deployment

Creation of deployment and service yaml files:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: special-node-label
                operator: In
                values:
                - frontend-node
      containers:
        - name: frontend
          image: frontend:0.1
          resources:
            requests:
              cpu: "100m"
              memory: "128Mi"
            limits:
              cpu: "500m"
              memory: "256Mi"
          ports:
            - containerPort: 80
```

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend-service
spec:
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 4000
    targetPort: 80
```

> **replicas** will create 2 different pods with the application.
>
> **affinity**	will define the node in which the pods will be created
>
> **requests** will set the virtualisation hardware requirements that will be used, and the limits on expantion.
>
> The service will serve the application at port 80, wile listening to port 4000 of the node.

**deployment:**

```shell
# locate workind dir
$ sudo kubctl apply -f .
```

------



### 2 / Back-end deployment

settingsCreation of deployment and service yaml files:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: special-node-label
                operator: In
                values:
                - backend-node
      containers:
        - name: backend
          image: backend:0.1
        - name: frontend
          image: frontend:0.1
          resources:
            requests:
              cpu: "500m"
              memory: "256Mi"
            limits:
              cpu: "1"
              memory: "1Gi"
          ports:
            - containerPort: 5000
          envFrom:
            - configMapRef:
                name: mongo-config 
          env:
            - name: MONGODB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: creds  
                  key: mongodb-password
```

```yaml
apiVersion: v1
kind: Service
metadata:
  name: backend-service
spec:
  selector:
    app: backend
  ports:
  - protocol: TCP
    port: 5000
    targetPort: 5000
```

> Similar settings as for the frontend, accept the node label that the pod will execute on.
>
> Important: the affinity is forcing the pod deployment, in case of miss-mach of the node names, it will not deploy at all.
>
> Env variables inherit URI credentials from config map  except the password which is in the "creds" secret map that we will create next.



**back end secret and configmap:**

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: creds
type: Opaque
data:
  mongodb-password: cGFzc3dvcmQx            # password1
```

> storing the password for MongoDB URI authentication in base64 :
>
> ```shell
> $ echo -n 'password1' | base64
> ```

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongo-config
data:
  MONGODB_MAIN_HOSTNAME: mongodb-0.mongodb-headless.database
  MONGODB_MAIN_HOSTNAME: mongodb-1.mongodb-headless.database
  MONGODB_PORT: "27017"
  MONGODB_DATABASE: exchange
  MONGODB_USERNAME: eric
```

> MongoDB URI authentication in the db will be established by pymongo **client** API, as such each instantiation will be connected and logged separately.



**Flask implementation:**

As seen earlier, the confection to each replica will be executed by separately, the execution of suchcan be seen here:

```python
# ... rest of the server

# Construct MongoDB URI:settings
db_usr = os.environ.get('MONGODB_USERNAME')
db_pswd = os.environ.get('MONGODB_PASSWORD')
db_host_main = os.environ.get('MONGODB_MAIN_HOSTNAME')
db_host_sec = os.environ.get('MONGODB_SEC_HOSTNAME')
db_port = int(os.environ.get('MONGODB_PORT'))
db_name = os.environ.get('MONGODB_DATABASE')

def push_to_db(collection):
    '''
    function to get the input from the frioont end in json format
    parse it to match the db format
    push it to the db with specified host, db name and collection.
    return the output (success or error with description)
    '''

    try:
        data = request.get_json()
        first_name = data['firstName']
        last_name = data['lastName']
        checkboxes = data.get('checkboxes', {})

        # Insert data into MongoDB
        user_data = {'first_name': first_name, 'last_name': last_name, 'checkboxes': checkboxes}
        result = collection.insert_one(user_data)

        return {
            "status": "success",
            "message": "Data received and stored successfully!"
        }
    except Exception as e:
        error_message = str(e)
        return {
            "status": "error",
            "message": "Error occurred: {}".format(error_message)
        }

#Connect to MongoDB
client_1 = MongoClient(host=db_host_main, port=db_port, username=db_usr, password=db_pswd)
client_2 = MongoClient(host=db_host_sec, port=db_port, username=db_usr, password=db_pswd)
clients = [client_1,client_2]

# on connection from client side:
@app.route('/log', methods=['POST'])
def log_data():
    results = list() # from db
    
    # establish connection for each db replica
    for client_est in clients:
        db = client_est['exchange']
        collection = db['user_data']
        
        # call function to push the data for each client
        result = push_to_db(collection)
        results.append(result)
        
    for item in results:
        return jsonify(item)

# ... rest of the server
```

**deployment:**

```shell
# locate workind dir
$ sudo kubctl apply -f .
```

------



### 3 / MongoDB StatefulSet

> StatefulSet is the workload API object used to manage stateful applications.Manages the deployment and scaling of a set of Pods, and provides guarantees about the ordering and uniqueness of these Pods.
>



In the process of setting up a MongoDB StatefulSet, several crucial  configuration files are employed to ensure a robust and secure  deployment:



1. **database-namespace.yaml**

```yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: database
```

> **Database-namespace** establishes a dedicated  namespace called "database," which helps isolate and manage the MongoDB  resources effectively.



2. **mongodb-secrets.yaml**

```yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: mongodb
  namespace: database
type: Opaque
data:
  mongodb-root-password: MTIzNDU2Nzg=       # 12345678
  mongodb-password: cGFzc3dvcmQx            # password1
  mongodb-replica-set-key: cmVwbGljYWtleQ== # replicakey

```

> **Secrets** file is essential for  safeguarding sensitive information such as root passwords, user  credentials, and replica set keys, shielding them from unauthorized  access.
>
> type: Opaque indicates that this Secret is an arbitrary collection of key-value pairs without  any specific interpretation by Kubernetes.
>
> Encoding in base64:
>
> ```shell
> $ echo -n 'password1' | base64
> ```



3. **mongodb-serviceaccount.yaml**

```yaml
---
apiVersion: v1
kind: ServiceAccount
  name: mongodb
  namespace: database
secrets:
- name: mongodb
```

> **Service account** defines a specialized service  account named "mongodb," which grants controlled permissions to the  MongoDB pods within the namespace.



4. **mongodb-scripts-configmap.yaml**

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-scripts
  namespace: database
data:
  setup.sh: |-
    #!/bin/bash

    echo "Advertised Hostname: $MONGODB_ADVERTISED_HOSTNAME"

    if [[ "$MY_POD_NAME" = "mongodb-0" ]]; then
        echo "Pod name matches initial primary pod name, configuring node as a primary"
        export MONGODB_REPLICA_SET_MODE="primary"
    else
        echo "Pod name doesn't match initial primary pod name, configuring node as a secondary"
        export MONGODB_REPLICA_SET_MODE="secondary"
    fi

    exec /opt/bitnami/scripts/mongodb/entrypoint.sh /opt/bitnami/scripts/mongodb/run.sh
```

> **Scripts-configmap** configures a ConfigMap containing  scripts that orchestrate the behavior of primary and secondary replicas, enhancing the efficiency of the stateful deployment.
>
> Replication provides data redundancy, fault tolerance, and high availability.
>
> Checks if the pod's name (`$MY_POD_NAME`) matches the name of the initial primary pod (`mongodb-0`). If it matches, the script sets an environment variable `MONGODB_REPLICA_SET_MODE` to "primary". if not stes the mode to "secondary" 



5. **mongodb-statefulset.yaml**

```yaml
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongodb
  namespace: database
spec:
  serviceName: mongodb-headless
  podManagementPolicy: OrderedReady
  replicas: 2
  selector:
    matchLabels:
      app.kubernetes.io/name: mongodb
      app.kubernetes.io/component: mongodb
  template:
    metadata:
      labels:
        app.kubernetes.io/name: mongodb
        app.kubernetes.io/component: mongodb
    spec:
      serviceAccountName: mongodb
      # avoid being scheduled on the same node:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/name: mongodb
              namespaces:
              - database
              topologyKey: kubernetes.io/hostname
            weight: 1
      containers:
      - name: mongodb
        image: mongo:5.1
        #Inheritance of environment variables:
        env:
        - name: MY_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: MY_POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: K8S_SERVICE_NAME
          value: "mongodb-headless"
        - name: MONGODB_INITIAL_PRIMARY_HOST
          value: "mongodb-0.$(K8S_SERVICE_NAME).$(MY_POD_NAMESPACE).svc.cluster.local"
        - name: MONGODB_ADVERTISED_HOSTNAME
          value: 		 "$(MY_POD_NAME).$(K8S_SERVICE_NAME).$(MY_POD_NAMESPACE).svc.cluster.local"
        - name: MONGODB_USERNAME
          value: "eric"
        - name: MONGODB_DATABASE
          value: "exchange"
        - name: MONGODB_REPLICA_SET_NAME
          value: "rs0"
        - name: AUTH
          value: "yes"
        - name: MONGODB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mongodb
              key: mongodb-password
        - name: MONGODB_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mongodb
              key: mongodb-root-password
        - name: MONGODB_REPLICA_SET_KEY
          valueFrom:
            secretKeyRef:
              name: mongodb
              key: mongodb-replica-set-key
          # Container resources:
          resources:
            requests:
              cpu: "500m"
              memory: "256Mi"
            limits:
              cpu: "1"
              memory: "1Gi"
        ports:
        - containerPort: 27017
          name: mongodb
        env:
        - name: MONGODB_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mongodb
              key: mongodb-root-password
        volumeMounts:
        ports:
        - name: metrics
          containerPort: 27017
      volumes:
      - name: scripts
        configMap:
          name: mongodb-scripts
          defaultMode: 0755
  # Persistent Volume Claim
  volumeClaimTemplates:
  - metadata:
      name: datadir
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 1Gi
```

> **Mongodb-statefulset** forms the core of the MongoDB setup, defining the StatefulSet resource that manages the deployment's replica pods.
>
> `serviceName`: Specifies the name of the headless service that controls the network identity of the MongoDB StatefulSet.
>
> `podManagementPolicy`: Defines the way pods are managed during scaling events (e.g., ordered and ready before proceeding).
>
> `replicas`: Specifies the desired number of pod replicas for the StatefulSet.
>
> `selector`: Specifies the labels used to match and manage the pods belonging to the StatefulSet.
>
> **avoid being scheduled on the same node:**
>
> `podAntiAffinity`: Specifies that pods should avoid being scheduled together.
> `preferredDuringSchedulingIgnoredDuringExecution`: Expresses the preferred scheduling behavior.
> `podAffinityTerm` : Defines the term for pod affinity.
> `labelSelector`: Selects pods based on labels.
> `namespaces`: Specifies the namespace where the labeled pods reside.
> `topologyKey`: Specifies the node property (hostname) used for affinity.
> `weight: 1`: Indicates the preference strength for this affinity rule.
>
> **Inheritance of environment variables:**
> The namespace, service name,  hostname of the initial primary MongoDB node, username for authentication, database name from the specified and created earlier  ConfigMap. password and replicaSet key from the specified and created earlier secret.
>
> configure the advertised hostname for the MongoDB node, the MongoDB database name and enable authentication for MongoDB.
>
> **Container resources:**
>
> `resources`: set the requested and limit values for CPU and memory for each MongoDB container.
>
> **Persistent Volume Claim:**
>
> `name: datadir`: Specifies the name of the volume claim template.
>
> `accessModes`: Defines the access mode for the volume (ReadWriteOnce allows read and write access by a single node).
>
> `resources.requests.storage: 1Gi`: Specifies that the PVC should request 1 Gibibyte (GiB) of storage.
>
> This PVC template ensures that each MongoDB pod will have a dedicated storage volume with the specified capacity
>
> In KinD, you can use the default storage provisioner, which simulates  persistent storage using the local filesystem of the host machine. KinD  will automatically mount host paths as persistent volumes within the  pods.

6. **mongodb-headless-svc.yaml**

```
---
apiVersion: v1
kind: Service
metadata:
  name: mongodb-headless
  namespace: database
spec:
  type: ClusterIP
  clusterIP: None
  publishNotReadyAddresses: true
  ports:
  - name: mongodb
    port: 27017
    targetPort: mongodb
  selector:
    app.kubernetes.io/name: mongodb
    app.kubernetes.io/component: mongodb
```

> **Headless service** establishes a headless service  that allows direct communication with individual pods, facilitating  seamless interactions and enabling a resilient MongoDB deployment in the Kubernetes environment.
>
> the `selector` referrers to the labels created earlier in the statefulSet yaml

**deployment:**

```shell
# locate workind dir
$ sudo kubctl apply -f .
```

------



### 4 / Results

After the deployments the cluster should have 6 running pods (2 of which in "database" name-space).

**Front/Back end pods:**

```shell
$ sudo kubectl get pods -o wide
```

```shell
NAME			READY	STATUS	RESTARTS	AGE		IP			NODE
backend-xxxxx1	1/1		Runing	0			10m		10.244.1.1	worker1
backend-xxxxx1	1/1		Runing	0			10m		10.244.1.2	worker1
frontkend-xxx1	1/1		Runing	0			10m		10.244.2.1	worker2
frontkend-xxx1	1/1		Runing	0			10m		10.244.2.2	worker2
```

**MongoDB pods:**

```shell
$ sudo kubectl get pods -n <namespace> -o wide
```

```shell
NAME			READY	STATUS	RESTARTS	AGE		IP			NODE
mongodb-0		1/1		Runing	0			10m		10.244.3.1	worker3
mongodb-1		1/1		Runing	0			10m		10.244.4.2	worker4
```



**Port-forwarding:**

> Port forwarding your frontend inside the cluster enables external access to the application during development or troubleshooting by allowing you to access the frontend server running in the Kubernetes cluster from your local machine without exposing it to the broader network.

```shell
# get the service name:
$ sudo kubectl get svc 
$ sudo kubectl port-forward svc/<service name> <listening port>:<destenation>
```

After this, and if every thing worked according to the plan, you should be able to enter the front end page,

send your information, and get a successful result if so.

`{"status": "success", "message": "Data received and stored successfully"}`



**Data insert Check:**

```shell
$ sudo kubectl exec --stdin --tty pod_name -n <namespace> -- mongo -u <usrname> -p <pswd>
```

> In this case, we will need to be able to enter our specified db and collection to read the data

1. Show the data-bases:

   ```
   > show dbs
   admin     0.000GB
   config    0.000GB
   exchange  0.000GB  <- non default db
   local     0.000GB
   ```

2. Use the db, and show the collections inside it:

   ```
   > use exchange
   > show collections
   user_data
   ```

3. Find the written data:

   ```
   > db.user_data.find()
   ```

**Result:**

```
{ "_id" : ObjectId("64e36dfd6a9de51af25a0c0d"), "first_name" : "name", "last_name" : "last", "checkboxes" : { "checkbox1" : true, "checkbox2" : false } }
```

<div align="center">
	<img src="https://i.pinimg.com/originals/c3/69/23/c36923fc3fcc6d4002e54406a91f254f.jpg" align="center" width="300" alt="Project icon" style="border: 5px solid  gray;"/>
</div>
------

