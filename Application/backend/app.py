from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
import os
import json

app = Flask(__name__)
CORS(app)

# Construct MongoDB URI:
db_usr = os.environ.get('MONGODB_USERNAME')
db_pswd = os.environ.get('MONGODB_PASSWORD')
db_host_main = os.environ.get('MONGODB_MAIN_HOSTNAME')
db_host_sec = os.environ.get('MONGODB_SEC_HOSTNAME')
db_port = os.environ.get('MONGODB_PORT')
db_name = os.environ.get('MONGODB_DATABASE')
output = True


def push_to_db(collection):
    '''
    function to get the input from the frioont end in json format
    parse it to match the db format
    push it to the db with specified host, db name and collection.
    return the output (success or error with description)
    '''

    global output

    try:
        data = request.get_json()
        first_name = data['firstName']
        last_name = data['lastName']
        checkboxes = data.get('checkboxes', {})

        # Insert data into MongoDB
        user_data = {'first_name': first_name, 'last_name': last_name, 'checkboxes': checkboxes}
        result = collection.insert_one(user_data)

        return {
            "status": "success",
            "message": "Data received and stored successfully!"
        }
    except Exception as e:
        error_message = str(e)
        return {
            "status": "error",
            "message": "Error occurred: {}".format(error_message)
        }



#Connect to MongoDB
client_1 = MongoClient(host=db_host_main, port=27017, username=db_usr, password=db_pswd)
client_2 = MongoClient(host=db_host_sec, port=27017, username=db_usr, password=db_pswd)
clients = [client_1,client_2]

# on connection from client side:
@app.route('/log', methods=['POST'])
def log_data():
    results = list()
    for client_est in clients:
        db = client_est['exchange']
        collection = db['user_data']
        # call function to push the data for each client
        result = push_to_db(collection)
        results.append(result)

        for item in results:
            return jsonify(item)

    
    # concatinate the massages and send to the web page:
    

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
