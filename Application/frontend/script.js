document.addEventListener("DOMContentLoaded", function () {
  const firstNameField = document.getElementById("firstName");
  const lastNameField = document.getElementById("lastName");
  const submitButton = document.getElementById("submitButton");
  const responseContainer = document.getElementById("responseContainer");
  const showCheckboxesButton = document.getElementById("showCheckboxesButton");
  const checkboxContainer = document.getElementById("checkboxContainer");
  const checkbox1 = document.getElementById("checkbox1");
  const checkbox2 = document.getElementById("checkbox2");
  

  submitButton.addEventListener("click", async () => {
    const firstName = firstNameField.value;
    const lastName = lastNameField.value;
    const checkboxes = {
      checkbox1: checkbox1.checked,
      checkbox2: checkbox2.checked,
    };

    try {
      const response = await fetch("http://localhost:5000/log", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ firstName, lastName, checkboxes }),
      });

      const data = await response.json();
      if (response.ok) {
        responseContainer.textContent = `Server response: ${data.message}`;
      } else {
        responseContainer.textContent = `Error: ${data.message}`;
        if (data.error) {
          responseContainer.textContent += ` - ${data.error}`;
        }
      }
    } catch (error) {
      console.error("Error:", error);
      responseContainer.textContent = "An error occurred. Please try again.";
    }
  });

  showCheckboxesButton.addEventListener("click", () => {
    if (checkboxContainer.style.display === "block") {
      checkboxContainer.style.display = "none";
    } else {
      checkboxContainer.style.display = "block";
    }
  });
});