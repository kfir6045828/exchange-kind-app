# <span style="color:#4863A0">Web App In Docker Compose</span>

> note: this file contain code and examples for docker compose configuration, note that the files that present in the repository are slightly modified for kubernetes cluster comparability
>

**Front-end:**

the front-end where developed in plain HTML, CSS and JS to later be imaged inside NGINX image, the front end spouse to have the following:

1. Entering user data, with 2 fields and 2 check boxes.
2. Ability to send the information to server.

**1 / Frontend:**

```javascript
document.addEventListener("DOMContentLoaded", function () {
  const firstNameField = document.getElementById("firstName");
  const lastNameField = document.getElementById("lastName");
  const submitButton = document.getElementById("submitButton");
  const responseContainer = document.getElementById("responseContainer");
  const showCheckboxesButton = document.getElementById("showCheckboxesButton");
  const checkboxContainer = document.getElementById("checkboxContainer");
  const checkbox1 = document.getElementById("checkbox1");
  const checkbox2 = document.getElementById("checkbox2");
  

  submitButton.addEventListener("click", async () => {
    const firstName = firstNameField.value;
    const lastName = lastNameField.value;
    const checkboxes = {
      checkbox1: checkbox1.checked,
      checkbox2: checkbox2.checked,
    };

    try {
      const response = await fetch("http://backend:5000/log", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ firstName, lastName, checkboxes }),
      });

      const data = await response.json();
      if (response.ok) {
        responseContainer.textContent = `Server response: ${data.message}`;
      } else {
        responseContainer.textContent = `Error: ${data.message}`;
        if (data.error) {
          responseContainer.textContent += ` - ${data.error}`;
        }
      }
    } catch (error) {
      console.error("Error:", error);
      responseContainer.textContent = "An error occurred. Please try again.";
    }
  });

  showCheckboxesButton.addEventListener("click", () => {
    if (checkboxContainer.style.display === "block") {
      checkboxContainer.style.display = "none";
    } else {
      checkboxContainer.style.display = "block";
    }
  });
});
```

> this java script will fetch the data to the backend at port 5000 the data in JSON format, and will print in the frontend any incoming errors from the server or browser.



**NGINX CORS configuration:**

```yaml
# nginx.conf
server {
    listen 80;

    server_name localhost;

    location / {
        add_header 'Access-Control-Allow-Origin' 'https://www.yourfrontenddomain.com';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'Authorization, Content-Type';

        add_header 'Access-Control-Allow-Credentials' 'true';

        root /usr/share/nginx/html;

        index index.html; 

        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Origin' $http_origin;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'Authorization, Content-Type';
            add_header 'Content-Length' 0;
            return 204;
        }

        # ... other location configurations ...
    }
}
```

> this congiuration will allow incoming and outcome requests to the backend.

**Frontend Dockerfile**

```dockerfile
# Use the official Nginx base image
FROM nginx:latest

# Remove the default configuration file
RUN rm /etc/nginx/conf.d/default.conf

# Copy your custom configuration file to the container
COPY nginx.conf /etc/nginx/conf.d/

# Copy your frontend files to the Nginx document root
COPY . /usr/share/nginx/html

RUN rm /usr/share/nginx/html/nginx.conf

# Expose port 80
EXPOSE 80

# Start Nginx
CMD ["nginx", "-g", "daemon off;"]

```

------



**1 / Frontend:**

```python
from flask import Flask, request, jsonify
from flask_cors import CORS
from pymongo import MongoClient
from bson import ObjectId
import os

app = Flask(__name__)
CORS(app)

# Connect to MongoDB
mongo_uri = os.environ.get('MONGO_URI')
client = MongoClient(mongo_uri) 
db = client['exchange_db']
collection = db['user_data']

@app.route('/log', methods=['POST'])
def log_data():
    try:
        data = request.get_json()
        first_name = data['firstName']
        last_name = data['lastName']
        checkboxes = data.get('checkboxes', {})

        # Insert data into MongoDB
        user_data = {'first_name': first_name, 'last_name': last_name, 'checkboxes': checkboxes}
        result = collection.insert_one(user_data)

        # Retrieve the inserted data from MongoDB using the inserted_id
        inserted_data = collection.find_one({'_id': result.inserted_id})

        # Convert ObjectId to string before sending as JSON
        inserted_data['_id'] = str(inserted_data['_id'])

        return jsonify({"message": "Data received and stored successfully!", "data": inserted_data}), 200
    except Exception as e:
        error_message = str(e)
        return jsonify({"message": "Error occurred", "error": error_message}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
```

> Imports necessary modules including Flask, CORS, MongoClient, and BSON for ObjectId handling.
>
> Sets up a Flask app and enables CORS for cross-origin requests.
>
> Connects to a MongoDB instance using a URI fetched from an environment variable.
>
> Defines an endpoint ' /log ' to receive POST requests, extracts JSON data, and stores it in the 'user_data' collection.
>
> Runs the app on host '0.0.0.0' and port 5000 when executed as the main script.



**Backend Dockerfile**

```dockerfile
# Use the official Python image as the base image
FROM python:3.9

# Set environment variables for Flask
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

# Install necessary packages
RUN pip install flask flask-cors pymongo

# Expose the Flask port
EXPOSE 5000

# Create and set the working directory
WORKDIR /app

# Copy your backend files into the container
COPY . /app

# Start the Flask app when the container runs
CMD ["flask", "run"]
```

> It uses the official Python 3.9 image as the base.
>
> Environment variables are set for Flask: `FLASK_APP` points to `app.py` and `FLASK_RUN_HOST` is set to `0.0.0.0`.
>
> Necessary packages (`flask`, `flask-cors`, `pymongo`) are installed using `pip`.
>
> The container exposes port `5000` to the host.
>
> The working directory is set to `/app`.
>
> Files from your backend directory are copied into the container's `/app` directory.
>
> The Flask app is started using the `flask run` command when the container runs.

------

**MongoDB:**

> Dockerfile:

```dockerfile
# Use the official MongoDB image as the base image
FROM mongo:5.0

# Set environment variables for MongoDB authentication (as root user)
ENV MONGO_INITDB_ROOT_USERNAME=root
ENV MONGO_INITDB_ROOT_PASSWORD=12345678
```

------

**docker-compose:**

```yaml
version: '2'

services:
  frontend:
    image: frontend:0.1
    ports:
      - "4000:80"  
    networks:
      - frontend_network  

  backend:
    image: backend:0.1
    ports:
      - "5000:5000"
    container_name: backend
    networks:
      - backend_network  

  mongodb:
    image: mongodb:0.1
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: 12345678
    networks:
      - backend_network  

networks:
  frontend_network:

  backend_network:
    driver: bridge
```

> Version 3.8 of Docker Compose is used.
>
> The "frontend" service runs an image tagged as frontend:0.1, exposes port 4000 on the host, and belongs to the frontend_network.
>
> The "backend" service runs an image tagged as backend:0.1, exposes port 5000 on the host, has a custom container name ("backend"), and belongs to the backend_network.
>
> The "mongodb" service runs an image tagged as mongodb:0.1, with a root username and password set as environment variables, and belongs to the backend_network.
>
> Two networks are defined: frontend_network and backend_network.
>
> The "frontend" service is connected to the frontend_network.
>
> The "backend" and "mongodb" services are connected to the backend_network.
>
> The backend_network uses the "bridge" driver for communication.
>
> This configuration orchestrates the deployment of frontend, backend, and MongoDB services using Docker Compose, allowing them to communicate within their respective networks.

------

